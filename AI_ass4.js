var ai = "O"; // AI hardcoded to O
var human = "X"; // human hardcoded to X
var playerImg = { // The images to represent X and O.
	'X': "url('cross.png')",
	'O': "url('circle.png')"
};
var turn = null; // The current player's turn (ai or human).
var boardState = null; // The tic-tac-toe board state as a two dimensional array [3][3].

// Change turn from the current player to the other.
function changeTurn() {
	if (turn == human) {
		turn = ai;
	} else if (turn == ai) {
		turn = human;
	}
}

// Get the opponent to 'player' (the other player).
function getOpponent(player) {
	if (player == "X") {
		return "O";
	} else if (player == "O") {
		return "X";
	}
}

// Returns a copy of the state.
function copyState(state) {
	var copy = state.slice(0);
	for (var row = 0; row < copy.length; row++) {
		copy[row] = state[row].slice(0);
	}
	return copy;
}

// Sets all tiles in 'state' to "".
function resetState(state) {
	for (var row = 0; row < state.length; row++) {
		for (var col = 0; col < state[row].length; col++) {
			state[row][col] = "";
		}
	}
}

// Returns all the possible actions from a player's 'turn'.
// An action is representet as an object with the attributes 'row', 'col', and 'turn'.
// 'turn' is either 'X' or 'O'.
function getPossibleActions(state, turn) {
	var actions = [];
	for (var row = 0; row < state.length; row++) {
		for (var col = 0; col < state[row].length; col++) {
			// Free tile.
			if (state[row][col] == "") {
				// Create the action
				var action = {
					'row':row,
					'col':col,
					'turn':turn
				};
				actions.push(action);
			}
		}
	}
	return actions;
}

// Returns the state created from doing an action.
function getState(currentState, action) {
	var state = copyState(currentState);
	state[action.row][action.col] = action.turn;
	return state;
}

// Returns the utility for the 'player'. 1 win, -1 lose, 0 draw (or free tiles).
function getUtility(state, player, opponent) {
	// Return's the value of the three if they are equal and not empty (""),
	// or false if not.
	function threeInARow(one, two, three) {
		if (one == two && two == three && one != "") {
			return one;
		}
		return false;
	}

	// Returns the score value based on the result (player or opponent).
	function getScore(result) {
		if (result == player) {
			return 1;
		} else if (result == opponent) {
			return -1;
		}
	}

	// Check each row.
	for (var row = 0; row < 3; row++) {
		var result = threeInARow(state[row][0], state[row][1], state[row][2]);
		if (result) {
			return getScore(result);
		}
	}
	// Check each column.
	for (var col = 0; col < 3; col++) {
		var result = threeInARow(state[0][col], state[1][col], state[2][col]);
		if (result) {
			return getScore(result);
		}
	}
	// Check first diagonal.
	var result = threeInARow(state[0][0], state[1][1], state[2][2]);
	if (result) {
		return getScore(result);
	}
	// Check second diagonal.
	var result = threeInARow(state[0][2], state[1][1], state[2][0]);
	if (result) {
		return getScore(result);
	}

	// No three in a row.
	return 0;
}

// Checks if a state is an end state (three in a row, or full board).
function isEndState(state) {
	// Three in a row = end state.
	if (getUtility(state, "X", "O") != 0) {
		return true;
	}
	// Free tile = not end state.
	for (var row = 0; row < state.length; row++) {
		for (var col = 0; col < state.length; col++) {
			if (state[row][col] == "") {
				return false;
			}
		}
	}
	// No free tile = end state.
	return true;
}

// Returns the action 'player' should do against 'opponent' from 'state'.
function minimax(state, player, opponent) {
	// The actual recursive algorithm with alpha-beta pruning.
	function alphaBetaMinimax(state, turn, alpha, beta, maxPlayer, minPlayer) {
		// Return the utility if the state is an end state (game over).
		if (isEndState(state)) {
			numEndStates += 1;
			return getUtility(state, maxPlayer, minPlayer);
		}
		var actions = getPossibleActions(state, turn);

		// Maximize.
		if (turn == maxPlayer) {
			var highestMinimax = -Infinity;
			for (var i = 0; i < actions.length; i++) {
				var nextState = getState(state, actions[i]);
				var minimax = alphaBetaMinimax(nextState, minPlayer, alpha, beta, maxPlayer, minPlayer)
				if (minimax > highestMinimax) {
					highestMinimax = minimax;
				}
				// MIN already has a better minimax, so no need to check further here.
				if (minimax >= beta) {
					return minimax;
				}
				// MAX found a better minimax.
				if (minimax > alpha) {
					alpha = minimax;
				}
			}
			return highestMinimax;
		}

		// Minimize.
		if (turn == minPlayer) {
			var lowestMinimax = Infinity;
			for (var i = 0; i < actions.length; i++) {
				var nextState = getState(state, actions[i]);
				var minimax = alphaBetaMinimax(nextState, maxPlayer, alpha, beta, maxPlayer, minPlayer)
				if (minimax < lowestMinimax) {
					lowestMinimax = minimax;
				}
				// MAX already has a better minimax, so no need to check further here.
				if (minimax <= alpha) {
					return minimax;
				}
				// MIN found a better minimax.
				if (minimax < beta) {
					beta = minimax;
				}
			}
			return lowestMinimax;
		}
	}

	// A counter used to check how many endstates were checked. Not part of the algorithm.
	var numEndStates = 0;
	
	// Find the best action for 'player'.
	var actions = getPossibleActions(state, player);
	var bestAction = null;
	var bestMinimax = -Infinity;
	var alpha = -Infinity; // Best value for max along the path to the root.
	var beta = Infinity; // Best value for min along the path to the root.
	for (var i = 0; i < actions.length; i++) {
		var nextState = getState(state, actions[i]);
		var minimax = alphaBetaMinimax(nextState, opponent, alpha, beta, player, opponent);
		if (minimax > bestMinimax) {
			bestMinimax = minimax;
			bestAction = actions[i];
		}
	}
	console.log(numEndStates);
	return bestAction;
}

// Pop up an alert telling the player who won.
function alertWinner(state) {
	var result = getUtility(state, "X", "O");
	if (result != 0) {
		var winner = "";
		if (result == 1) {
			winner = "X";
		} else {
			winner = "O";
		}
		alert("" + winner + " wins!");
	} else {
		alert("It's a draw!");
	}
}

function doAiTurn() {
	// A small timeout to make the ai look like it's thinking.
	setTimeout(
		function() {
			var aiAction = minimax(boardState, ai, human);
			boardState[aiAction.row][aiAction.col] = aiAction.turn;
			updateBoard(boardState);
			turn = human;
			if (isEndState(boardState)) {
				alertWinner(boardState);
				startGame();
				return;
			}
		},
		300
	);
}

// Called when a box (tile) is clicked by the user.
function boxSelection( box ) {
	if (turn == human) {
		if (boardState[box.dataset.row][box.dataset.col] == "") {
			// Update the board state with the tile clicked.
			boardState[box.dataset.row][box.dataset.col] = human;
			updateBoard(boardState);
			if (isEndState(boardState)) {
				alertWinner(boardState);
				startGame();
				return;
			}
			turn = ai;
			doAiTurn();
		} else {
			console.log("W: Box already taken.");
		}
	} else {
		console.log("W: Not your turn.");
	}
}

// Updates thw visible board on the page from the 'state'.
function updateBoard(state) {
	for (var row = 0; row < state.length; row++) {
		for (var col = 0; col < state[row].length; col++) {
			var player = state[row][col];
			// Not a free tile.
			if (player != "") {
				// Draw the image of the player to the board.
				document.getElementById("cell_" + row + "-" + col).style.backgroundImage = playerImg[player];
			} else {
				// remove the image.
				document.getElementById("cell_" + row + "-" + col).style.backgroundImage = "";
			}
		}
	}
}

// Start the game
function startGame() {
	// Reset game
	resetState(boardState);
	updateBoard(boardState);
	// Check which player that should start.
	var starter = document.getElementsByName("startAgent");
	for( var i = 0; i < starter.length; i++ ) {
		if( starter[i].checked ) {
			if( starter[i].value == "AI" ) {
				// AI, so do the turn.
				turn = ai;
				doAiTurn();
			} else if( starter[i].value == "Player" ) {
				// Human, so just wait for input.
				turn = human;
			}
		}
	}
}

// Initialize the game.
function init() {
	// Create the board state.
	boardState = new Array(3);
	for (var row = 0; row < boardState.length; row++) {
		boardState[row] = new Array(3);
		for (var col = 0; col < boardState[row].length; col++) {
			boardState[row][col] = "";
		}
	}

	// Add event listener for all the tiles.
	var boxes = document.getElementsByClassName("checkbox");
	for( var i = 0; i < boxes.length; i++ )
	{
		boxes[i].onclick = function() {
			boxSelection( this );
		}
	}

	document.getElementById("startbutton").onclick = startGame;
	startGame();

	// Testing of ai vs ai.
	// while (isEndState(boardState) == false) {
	// 	var action = minimax(boardState, turn, getOpponent(turn));
	// 	boardState[action.row][action.col] = action.turn;
	// 	updateBoard(boardState);
	// 	changeTurn();
	// }
} // end init()
