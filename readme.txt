Open index.html in a web browser. (Tested with chrome and firefox)
Start by clicking a cell with your pointer.
You can start over by pressing "Start".
You can choose who starts by selecting 'Player' or 'AI' and then pressing "Start".
The number of terminal states encountered by minimax is logged to the console in
the browser (F12 in chrome).
